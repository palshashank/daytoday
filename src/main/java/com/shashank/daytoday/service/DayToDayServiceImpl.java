package com.shashank.daytoday.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shashank.daytoday.dao.DayToDayDao;
import com.shashank.daytoday.model.Person;

@Service
public class DayToDayServiceImpl  implements DayToDayService{

	@Autowired
	DayToDayDao dao;
	
	private static final Logger logger = LoggerFactory.getLogger(DayToDayServiceImpl.class);


	@Override
	public void persistPerson(Person person) throws Exception{
		logger.info("INSIDE persistPerson()");
		try {
			String orgDocId = dao.persistOrganization(person.getOrganizationName(), person.getOrganizationAddress());
			logger.info("Persist person method | Orgainzation Doc persisted ");
			dao.persistPerson(person, orgDocId);
			logger.info("Persist person method | Person doc persisted");
		} catch (Exception e) {
			logger.error("INSIDE persist Person");
			logger.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		
	}


	@Override
	public List<Person> getAllPersons() throws Exception {
		logger.info("INSIDE getAllPersons()");
		try {		
			return dao.fetchAllPersons();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
	
	}


	@Override
	public List<Person> findPersonByNameAndOrg(String firstName, String lastName, String orgName) throws Exception {
		logger.info("INSIDE findPersonByNameAndOrg()");
		try {
			return dao.findPerson(firstName, lastName, orgName);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		
	}


	
}
