package com.shashank.daytoday.service;

import java.util.List;

import com.shashank.daytoday.model.Person;

public interface DayToDayService {
	
	public List<Person> getAllPersons() throws Exception;
	
	public void persistPerson(Person person) throws Exception;
	
	public List<Person> findPersonByNameAndOrg(String firstName, String lastName, String orgName) throws Exception;
	

}
