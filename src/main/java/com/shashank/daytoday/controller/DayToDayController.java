package com.shashank.daytoday.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shashank.daytoday.model.Person;
import com.shashank.daytoday.model.Response;
import com.shashank.daytoday.service.DayToDayService;


@RestController(value = "/api")
public class DayToDayController {
	
	@Autowired
	DayToDayService service;
		
	@GetMapping(value = "/person")
	public ResponseEntity<Response<List<Person>>> getEmployees() {
		Response<List<Person>> response = new Response<>();
		try {
			List<Person> personList = service.getAllPersons();
			response.setStatus(HttpStatus.OK);
			response.setResponse(personList);
			response.setMessage("Data fetched successfully");
			return ResponseEntity.ok().body(response);
		} catch (Exception e) {
			response.setResponse(null);
			response.setMessage("Unable to fetch results");
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		
	}
	
	@PostMapping(value = "/person")
	public ResponseEntity<Response<String>> persistPerson(@RequestBody Person request) {
		Response<String> response = new Response<>();
		try {
			service.persistPerson(request);
			response.setMessage("Data Persisted successfully");
			response.setStatus(HttpStatus.OK);
			response.setResponse(null);
			return ResponseEntity.ok().body(response);
		} catch (Exception e) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Unable to persist data. Please try again later.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		
		
	}
	
	@GetMapping(value = "/person/{fullName}")
	public ResponseEntity<Response<List<Person>>> getPerson(@PathVariable String fullName, @RequestParam String orgName){
		
		String[] name = fullName.split("_");
		
		Response<List<Person>> response = new Response<>();
		try {
			response.setStatus(HttpStatus.OK);
			response.setMessage("Data fetched successfully");
			List<Person> person = service.findPersonByNameAndOrg(name[0], name[1], orgName);
			response.setResponse(person);
			return ResponseEntity.ok().body(response);
		} catch (Exception e) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Unable to fetch. Please try again later.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		
	}
	
	

}
