package com.shashank.daytoday.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.shashank.daytoday.entity.PersonEntity;

public interface PersonRepository extends MongoRepository<PersonEntity, String>{
	
	@Query(value = "{'name.given': ?0 , 'name.family': ?1}")
	public Optional<List<PersonEntity>> findByFirstNameAndLastName(String firstName, String lastName);

}
