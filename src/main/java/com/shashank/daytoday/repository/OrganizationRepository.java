package com.shashank.daytoday.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.shashank.daytoday.entity.OrganizationEntity;

public interface OrganizationRepository extends MongoRepository<OrganizationEntity, String>{
	
	public Optional<OrganizationEntity> findByNameAndId(String name, String Id);

}
