package com.shashank.daytoday.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.shashank.daytoday.entity.AddressEntity;
import com.shashank.daytoday.entity.HumanNameEntity;
import com.shashank.daytoday.entity.OrganizationEntity;
import com.shashank.daytoday.entity.PersonEntity;
import com.shashank.daytoday.model.Address;
import com.shashank.daytoday.model.Person;
import com.shashank.daytoday.repository.OrganizationRepository;
import com.shashank.daytoday.repository.PersonRepository;

@Repository
public class DayToDayDaoImpl implements DayToDayDao {

	@Autowired
	OrganizationRepository orgRepo;

	@Autowired
	PersonRepository personRepo;

	private static final Logger logger = LoggerFactory.getLogger(DayToDayDaoImpl.class);

	@Override
	public String persistOrganization(String name, Address address) throws Exception {
		OrganizationEntity entity = new OrganizationEntity();
		AddressEntity addressEntity = new AddressEntity();
		if (address != null) {
			addressEntity = new AddressEntity(address);
		}
		entity.setName(name);
		entity.setAddress(addressEntity);
		return orgRepo.save(entity).getId();
	}

	@Override
	public String persistPerson(Person person, String orgDoc) throws Exception {
		PersonEntity entity = new PersonEntity();
		HumanNameEntity name = new HumanNameEntity();
		AddressEntity addressEntity = new AddressEntity();
		if (person.getAddress() != null) {
			addressEntity = new AddressEntity(person.getAddress());
		}
		name.setGiven(person.getFirstName());
		name.setFamily(person.getLastName());

		entity.setName(name);

		entity.setGender(person.getGender());
		entity.setBirthDate(person.getBirthDate());
		entity.setAddress(addressEntity);
		entity.setManagingOrganization(orgDoc);

		return personRepo.save(entity).getId();
	}

	@Override
	public List<Person> fetchAllPersons() throws Exception {

		List<Person> response = new ArrayList<>();
		List<PersonEntity> personEntityList = personRepo.findAll();

		for (PersonEntity personEntity : personEntityList) {
			Person person = new Person();
			person.setFirstName(personEntity.getName().getGiven());
			person.setLastName(personEntity.getName().getFamily());
			Address address = new Address(personEntity.getAddress());
			person.setAddress(address);
			person.setGender(personEntity.getGender());
			person.setBirthDate(personEntity.getBirthDate());
			try {
				Optional<OrganizationEntity> organization = orgRepo.findById(personEntity.getManagingOrganization());
				if (organization.isPresent()) {
					person.setOrganizationName(organization.get().getName());
					Address orgAddress = new Address(organization.get().getAddress());
					person.setOrganizationAddress(orgAddress);
				}
			} catch (Exception e) {
				logger.warn("Organization reference is missing.");
			}
			response.add(person);
		}

		return response;
	}

	@Override
	public List<Person> findPerson(String firstName, String lastName, String orgName) throws Exception {

		List<Person> personList = new ArrayList<>();

		Optional<List<PersonEntity>> entity = personRepo.findByFirstNameAndLastName(firstName, lastName);

		for (PersonEntity personEntity : entity.get()) {
			Person person = new Person();
			OrganizationEntity organizationEntity = null;
			person.setFirstName(personEntity.getName().getGiven());
			person.setLastName(personEntity.getName().getFamily());
			Address address = new Address(personEntity.getAddress());
			person.setAddress(address);
			person.setGender(personEntity.getGender());
			person.setBirthDate(personEntity.getBirthDate());
			
			Optional<OrganizationEntity> org = orgRepo.findByNameAndId(orgName, personEntity.getManagingOrganization());
			if (!org.isPresent()) {
				continue;
			}
			organizationEntity = org.get();

			person.setOrganizationName(organizationEntity.getName());
			Address orgAddress = new Address(organizationEntity.getAddress());
			person.setOrganizationAddress(orgAddress);

			personList.add(person);
		}
		return personList;
	}

}
