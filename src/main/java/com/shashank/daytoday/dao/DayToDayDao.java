package com.shashank.daytoday.dao;

import java.util.List;

import com.shashank.daytoday.model.Address;
import com.shashank.daytoday.model.Person;

public interface DayToDayDao {
	
	public List<Person> fetchAllPersons() throws Exception;
	
	public String persistPerson(Person person, String orgDoc) throws Exception;

	public String persistOrganization(String name, Address address) throws Exception;
	
	public List<Person> findPerson(String firstName, String lastName, String orgName) throws Exception;
	
}
