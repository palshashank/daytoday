package com.shashank.daytoday.entity;

public enum AddressType {
	postal, physical, both
}
