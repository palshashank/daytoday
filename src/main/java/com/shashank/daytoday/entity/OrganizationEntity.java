package com.shashank.daytoday.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Organization")
public class OrganizationEntity {
	
	@Id
	private String id;

	private String name;
	
	private AddressEntity address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return "OrganizationEntity [id=" + id + ", name=" + name + ", address=" + address + "]";
	}
	
	
}
