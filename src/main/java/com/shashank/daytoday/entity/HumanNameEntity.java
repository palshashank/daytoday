package com.shashank.daytoday.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class HumanNameEntity {

	
	enum HumanNameUse {
		usual, official, temp, nickname, anonymous, old, maiden
	}
	
	@Id
	private String id;
	
	private HumanNameUse use;
	
	private String text;
	
	private String family;
	
	private String given;
	
	private String prefix; 
	
	private String suffix;
	
	private Period period;

	public HumanNameUse getUse() {
		return use;
	}

	public void setUse(HumanNameUse use) {
		this.use = use;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getGiven() {
		return given;
	}

	public void setGiven(String given) {
		this.given = given;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}
	

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return "HumanNameEntity [id=" + id + ", use=" + use + ", text=" + text + ", family=" + family + ", given="
				+ given + ", prefix=" + prefix + ", suffix=" + suffix + ", period=" + period + "]";
	}
	
	
}
