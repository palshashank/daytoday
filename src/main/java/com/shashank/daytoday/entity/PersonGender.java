package com.shashank.daytoday.entity;

public enum PersonGender {
	male, female, other, unknown
}
