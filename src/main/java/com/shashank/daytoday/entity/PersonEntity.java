package com.shashank.daytoday.entity;

import java.time.LocalDate;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Person")
public class PersonEntity {
	
	@Id
	private String id;
	
	private HumanNameEntity name;
	
	private PersonGender gender;
	
	private LocalDate birthDate;
	
	private AddressEntity address;
	
	private String managingOrganization;

	public HumanNameEntity getName() {
		return name;
	}

	public void setName(HumanNameEntity name) {
		this.name = name;
	}

	public PersonGender getGender() {
		return gender;
	}

	public void setGender(PersonGender gender) {
		this.gender = gender;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public String getManagingOrganization() {
		return managingOrganization;
	}

	public void setManagingOrganization(String managingOrganization) {
		this.managingOrganization = managingOrganization;
	}

	public String getId() {
		return id;
	}
	
	

}
