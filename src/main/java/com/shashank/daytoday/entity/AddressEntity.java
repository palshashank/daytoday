package com.shashank.daytoday.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.shashank.daytoday.model.Address;

@Document
public class AddressEntity {

	@Id
	private String id;

	private AddressUse use;

	private AddressType type;

	private String text;

	private String line;

	private String city;

	private String district;

	private String state;

	private String postalCode;

	private String country;

	private Period period;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public AddressUse getUse() {
		return use;
	}

	public void setUse(AddressUse use) {
		this.use = use;
	}

	public AddressType getType() {
		return type;
	}

	public void setType(AddressType type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public AddressEntity(Address address) {
		this.city = address.getCity();
		this.country = address.getCountry();
		this.district = address.getDistrict();
		this.line = address.getLine();
		this.postalCode = address.getPostalCode();
		this.state = address.getState();
		this.text = address.getText();
		this.type = address.getType();
		this.use = address.getUse();
	}
	

	public AddressEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "AddressEntity [use=" + use + ", type=" + type + ", text=" + text + ", line=" + line + ", city=" + city
				+ ", district=" + district + ", state=" + state + ", postalCode=" + postalCode + ", country=" + country
				+ ", period=" + period + "]";
	}

}
