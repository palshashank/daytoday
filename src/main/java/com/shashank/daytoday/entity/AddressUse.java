package com.shashank.daytoday.entity;

public enum AddressUse {
	home, work, temp, old, billing
}
