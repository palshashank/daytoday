package com.shashank.daytoday.model;

import java.time.LocalDate;

import com.shashank.daytoday.entity.PersonGender;

public class Person {
	
	private String firstName;
	
	private String lastName;
	
	private PersonGender gender;
	
	private LocalDate birthDate;
	
	private Address address;
	
	private String organizationName;
	
	private Address organizationAddress;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public PersonGender getGender() {
		return gender;
	}

	public void setGender(PersonGender gender) {
		this.gender = gender;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Address getOrganizationAddress() {
		return organizationAddress;
	}

	public void setOrganizationAddress(Address organizationAddress) {
		this.organizationAddress = organizationAddress;
	}
	
	

}
