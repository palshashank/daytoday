package com.shashank.daytoday.model;

import com.shashank.daytoday.entity.AddressEntity;
import com.shashank.daytoday.entity.AddressType;
import com.shashank.daytoday.entity.AddressUse;

public class Address {

	
	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	private AddressUse use;

	private AddressType type;

	private String text;

	private String line;

	private String city;

	private String district;

	private String state;

	private String postalCode;

	private String country;

	public AddressUse getUse() {
		return use;
	}

	public void setUse(AddressUse use) {
		this.use = use;
	}

	public AddressType getType() {
		return type;
	}

	public void setType(AddressType type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public Address(AddressEntity address) {
		this.city = address.getCity();
		this.country = address.getCountry();
		this.district = address.getDistrict();
		this.line = address.getLine();
		this.postalCode = address.getPostalCode();
		this.state = address.getState();
		this.text = address.getText();
		this.type = address.getType();
		this.use = address.getUse();
	}

	@Override
	public String toString() {
		return "Address [use=" + use + ", type=" + type + ", text=" + text + ", line=" + line + ", city=" + city
				+ ", district=" + district + ", state=" + state + ", postalCode=" + postalCode + ", country=" + country
				+ "]";
	}
	
	

}
