package com.shashank.daytoday.model;

import org.springframework.http.HttpStatus;

public class Response<T> {
	
	private HttpStatus status;
	private String message;
	private T response;
	
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public T getResponse() {
		return response;
	}
	public void setResponse(T response) {
		this.response = response;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
